<?php
namespace app\api\model;

use app\common\model\BaseModel;

class UserExt extends BaseModel{
    protected $autoWriteTimestamp = true;
    //判断身份证是否存在
    public function checkAuthenticate($user_id,$idCard){
        $obj = $this->where(['user_id'=>$user_id,'idCard'=>$idCard])->find();
        if(empty($obj)){
            return false;
        }else{
            return true;
        }
    }
    //写入身份证信息
    public function saveAuthenticate($user_id,$idCard,$realName,$client_ip,$data){
        return $this->save([
            'user_id' => $user_id,
            'store_id' => self::$storeId,
            'client_ip' => $client_ip,
            'realName' => $realName,
            'idCard' => $idCard,
            'authenticate' => json_encode($data)
        ]);
    }

}
