<?php
namespace app\api\model;

use app\api\model\wine\Dermember;
use app\common\model\BaseModel;
use app\store\model\UploadFile;

class Poster extends BaseModel{
    protected $autoWriteTimestamp = true;

    //关联图片
    public function image(){
        return $this->hasOne(UploadFile::class,'file_id','imagesIds')
            ->bind(['domain','file_path']);
    }

    public function detail(){
        return self::where(['store_id'=>getStoreId()])
            ->with('image')
            ->find();
    }

    //注册
    public function register($params){
        $der = new Dermember();
        $r = $der->where(['idCard'=>$params['idCard']])->findOrEmpty();
        if($r->isEmpty()){
            return $der->save([
                'store_id' => getStoreId(),
                'pid'=> $params['pid'],
                'idCard'=>$params['idCard']
            ]);
        }
        return false;
    }
}
