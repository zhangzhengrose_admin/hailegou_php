<?php
namespace app\api\model;

use app\api\model\wine\Dermember;
use app\common\model\BaseModel;

class CashRequest extends BaseModel{
    protected $autoWriteTimestamp = true;

    /*public function dermember($user_id){
        return $this->hasOne(Dermember::class,'user_id','user_id')
            ->bind(['balance','cash','revenue','total_expend_money']);
    }*/
    //添加提现申请记录
    public function add($user_id,$cash_r){
        $dermember = new Dermember();
        $balance = $dermember->where(['user_id'=>$user_id])->value('balance');
        //提现金额大于余则提现失败
        if($cash_r>$balance){
            return false;
        }
        return $this->transaction(function () use($user_id,$cash_r,$dermember){
            $dermember->setDec(['user_id'=>$user_id],'balance',$cash_r);
            $dermember->setInc(['user_id'=>$user_id],'cash',$cash_r);
            return $this->save([
                'user_id' => $user_id,
                'store_id' => getStoreId(),
                'cash_r' => $cash_r
            ]);
        });
    }

    //改变提现记录状态
    public function setStatus($id,$status){
        return $this->where(['id'=>$id])->save([
            'status' => $status
        ]);
    }
}
