<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;
use app\store\model\store\Income;

class Dermember extends BaseModel{
    protected $autoWriteTimestamp = true;
    //关联用户表
    public function user(){
        return $this->hasOne(User::class,'user_id','user_id')
            ->bind(['mobile','nick_name']);
    }
    //关联经销商级别配置表
    public function perform(){
        return $this->belongsTo(Perform::class,'perform_id','id')
            ->bind(['name','award','des']);
    }
    //添加
    public function add($param){
        $data = [
            'store_id' => self::$storeId,
            'pid'=>$param['pid'],
            'user_id'=>$param['user_id'],
            'idCard'=>$param['idCard']
        ];
        $user_id = $param['user_id'];
        if(isset($param['id'])){
            return self::update($data,['id'=>$param['id']]);
        }else{
            //创建经销商，如果身份证相同则更新数据
            $performModel = new Perform();
            $incomeModel = new Income();
            $existDerModel = $this->where(['user_id'=>$user_id])->findOrEmpty();
            return $this->transaction(function () use ($data, $performModel, $incomeModel, $user_id, $existDerModel) {
                if ($existDerModel->isEmpty()) {
                    //新注册则添加一条直推记录
                    $pid = $data['pid'];
                    // 查询推荐者总消费额和团队人数
                    $r = $this->where(['id' => $pid])->field('total_expend_money,pcount')->find();
                    $performConfig = $performModel->getLevel($r['total_expend_money'],$r['pcount']);
                    $direct_award = $performConfig['direct_award'];
                    //写入一条直推记录
                    $incomeModel->directRecord($direct_award, $user_id);
                    $data['revenue'] = $direct_award;
                    return $existDerModel->save($data);
                }else{
                    // 已经注册则返回
                    return true;
                }
            });
        }
    }
    //扣减提现金额
    public function cash($cash,$user_id){
        //往经销商提现字段添加提现金额记录
        return $this->setInc(['user_id'=>$user_id],'cash',$cash);
    }
    //获取经销商子团队
    public function getSonDer($user_id,$line=10,$perform_id=0){
        $where['pid'] = $user_id;
        if($perform_id>0){
            $where['perform_id'] = $perform_id;
        }
        return self::where($where)
            ->with(['user','perform'])
            ->paginate($line);
    }
    public function getSonDer2($user_id,$perform_id=0,$line=15){
        $where['pid'] = $user_id;
        if($perform_id>0){
            $where['perform_id'] = $perform_id;
        }
        return self::where($where)
            ->with(['user','perform'])
            ->paginate($line);
    }
    //根据用户user_id查询经销商信息
    public function getDerInfo($user_id){
        return self::where(['user_id'=>$user_id,'store_id'=>self::$storeId])
            ->with(['user','perform'])
            ->find();
    }
    //判断是否注册经销商
    public function isRegist($user_id){
        $r = self::where(['user_id'=>$user_id])->findOrEmpty();
        return !$r->isEmpty();
    }

    // 生成19位随机数
    public function GetRandStr($length){
        //字符组合
        $str = '0123456789';
        $len = strlen($str)-1;
        $randstr = '';
        for ($i=0;$i<$length;$i++) {
            $num=mt_rand(0,$len);
            $randstr .= $str[$num];
        }
        return $randstr;
    }
    //实名认证后更新身份证号码
    public function updateIdCard($user_id,$idCard){
        return $this->where(['user_id'=>$user_id])->save([
            'idCard' => $idCard,
            'authens' => 1
        ]);
    }
    //查询经销商等级
    public function getLevelName($user_id){
        $r = self::where(['user_id'=>$user_id])->with('perform')->findOrEmpty();
        if($r->isEmpty()){
            return '-';
        }
        return $r['name'];
    }
}
