<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class Cash extends BaseModel{

    protected $autoWriteTimestamp = true;

    //写入提现记录
    public function cashRecord($cash,$user_id){
        $data = [
            'user_id' => $user_id,
            'store_id' => self::$storeId,
            'cash' => $cash
        ];
        return $this->save($data);
    }
    //提现明细
    public function getList($user_id,$page,$line){
        return $this->where(['user_id'=>$user_id])
            ->page($page,$line)
            ->order('create_time desc')
            ->select();
    }
}
