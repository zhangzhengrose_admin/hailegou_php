<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class Perform extends BaseModel{
    // 定义表名
    protected $name = 'perform';

    // 定义主键
    protected $pk = 'id';

    //获取经销商个人达标奖金
    private function getDBAward($performance,$pcount){
        $arr = $this->order('performance desc')->select();
        foreach ($arr as $value){
            if($performance>$value['performance'] && $pcount >= $value['pcount']){
                return $value;
            }
        }
        $lastIndex = count($arr) - 1;
        return $arr[$lastIndex];
    }
    //经销商级别数据
    public function getLevel($performance,$pcount,$perform_id_set=0){
        if($perform_id_set == 0){
            return $this->getDBAward($performance,$pcount);
        }else{
            return $this->where(['id'=>$perform_id_set])->find();
        }
    }
    //经销商列表
    public function getList($page,$line){
        return $this->page($page,$line)->select();
    }
}
