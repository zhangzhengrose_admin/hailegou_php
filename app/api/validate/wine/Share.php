<?php
namespace app\api\validate\wine;

use think\Validate;

class Share extends Validate{
    protected $rule =   [
        'pid'  => 'require',
        'idCard' => 'require',
        'name' => 'require',
    ];

    protected $message  =   [
        'pid.require' => '推荐者id必须',
        'idCard.require' => '身份证号码必须',
        'name.require' => '姓名必须',
    ];
    protected $scene = [
        'share'  =>  ['pid','idCard','name']
    ];
}
