<?php
namespace app\api\validate\wine;

use think\Validate;

class Cash extends Validate{
    protected $rule =   [
        'cash_r'  => 'require',
    ];

    protected $message  =   [
        'cash_r.require' => '提现金额必须',
    ];
    protected $scene = [
        'add'  =>  ['cash_r']
    ];
}
