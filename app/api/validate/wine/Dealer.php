<?php
namespace app\api\validate\wine;

use think\Validate;

class Dealer extends Validate{
    protected $rule =   [
        'pid'  => 'require',
        'idCard'   => 'require',
        'user_id'  => 'require',
        'income_id' => 'require'
    ];

    protected $message  =   [
        'pid.require' => '经销商推荐者id必须',
        'idCard.require'     => '身份证号码必须',
        'user_id.require'     => '用户user_id必须',
        'income_id.require' => '缺少income_id参数'
    ];
    protected $scene = [
        'add'  =>  ['pid','idCard'],
        'derinfo' => ['user_id'],
        'searchOrder' => ['income_id']
    ];
}
