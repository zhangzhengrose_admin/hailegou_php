<?php
namespace app\api\controller;

use think\App;
use app\api\model\Poster as PosterModel;
use app\api\validate\wine\Share;
use app\common\service\Authenticate;

class Poster extends Controller {

    private $posterModel;
    private $userArr = [];

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->userArr = $this->getLoginUser();
        $this->posterModel = new PosterModel();
    }

    //推荐接口
    public function share(){
        $user_id = $this->userArr['user_id'];
        $detail = $this->posterModel->detail();
        $detail['url'] = $detail['url']."?pid={$user_id}";
        $domain = $detail['domain'];
        $file_path = $detail['file_path'];
        $derUrl = $domain.'/'.$file_path;
        $detail['derUrl'] = $derUrl;
        unset($detail['domain']);
        unset($detail['file_path']);
        unset($detail['id']);
        unset($detail['store_id']);
        unset($detail['file_path']);
        unset($detail['imagesIds']);
        unset($detail['is_delete']);
        unset($detail['create_time']);
        unset($detail['update_time']);
        return $this->renderSuccess(compact('detail'));
    }
    //推荐注册经销商
    public function regist(){
        $params = $this->postForm();
        $shareValidate = new Share();
        if(!$shareValidate->scene('share')->check($params)){
            return $this->renderError($shareValidate->getError());
        }
        $r = Authenticate::sendAuthenticate('authenticate',[
            'name' => $params['name'],
            'idNo' => $params['idCard']
        ]);
        if($r->response->respCode == '0000'){
            $r = $this->posterModel->register($params);
            if($r){
                return $this->renderSuccess('注册成功');
            }else{
                return $this->renderError('注册失败,你已经是经销商,无需注册');
            }
        }else{
            return $this->renderError('注册失败,'.$r->response->respMessage);
        }
    }
}
