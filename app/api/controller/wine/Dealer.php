<?php
namespace app\api\controller\wine;


use app\api\controller\Controller;
use app\api\model\wine\Dermember as DermemberModel;
use app\api\model\wine\Perform;
use app\store\model\store\Income;
use app\api\model\Order;
use think\App;
use app\store\model\UploadFile as UploadFileModel;
use app\common\library\storage\Driver as StorageDriver;
use app\common\enum\file\FileType as FileTypeEnum;
use app\store\model\Setting as SettingModel;
use app\common\enum\Setting as SettingEnum;

class Dealer extends Controller {
    private $userArr = [];
    private $DerMermModer;
    private $IncomeModel;
    private $OrderModel;
    private $config;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->userArr = $this->getLoginUser();
        $this->DerMermModer = new DermemberModel();
        $this->IncomeModel = new Income();
        $this->OrderModel = new Order();
        // 存储配置信息
        $this->config = SettingModel::getItem(SettingEnum::STORAGE);
    }

    //注册经销商
    /*public function regist(){
        $data = $this->postForm();
        $user_id = $this->userArr['user_id'];
        $data['user_id'] = $user_id;
        $addValidate = new \app\api\validate\wine\Dealer();
        if(!$addValidate->scene('add')->check($data)){
            return $this->renderError($addValidate->getError());
        }
        if($this->DerMermModer->add($data)){
            return $this->renderSuccess([],'经销商成功注册或更新');
        }else{
            return $this->renderError('经销商注册失败');
        }
    }*/

    //经销商收益明细
    public function index(){
        $page = input('page',1);
        $line = input('line',10);
        $type = input('type',1);
        $user_id = $this->userArr['user_id'];
        $list = $this->IncomeModel->getList($user_id,$page,$line,$type);
        return $this->renderSuccess(compact('list'));
    }

    //经销商等级列表
    public function getDerLevel(){
        $performModel = new Perform();
        $page = input('page',1);
        $line = input('line',10);
        $list = $performModel->getList($page,$line);
        return $this->renderSuccess(compact('list'),'成功获取经销商等级列表');
    }

    /*//根据经销商等级查询我的经销商团队
    public function getDerLevelMember(){

    }*/

    //经销商信息查询
    public function derInfo(){
        $params = $this->postForm();
        $addValidate = new \app\api\validate\wine\Dealer();
        if(!$addValidate->scene('derinfo')->check($params)){
            return $this->renderError($addValidate->getError());
        }
        $derinfo = $this->DerMermModer->getDerInfo($params['user_id']);
        if(empty($derinfo)){
            return $this->renderError('获取经销商信息失败');
        }
        return $this->renderSuccess(compact('derinfo'),'经销商信息获取成功');
    }

    //我的经销商团队
    public function getDermember(){
        $line = input('line',10,'int');
        $perform_id = input('perform_id',0,'int');
        $user_id = $this->userArr['user_id'];
        if(!$this->isRegistDer($user_id)){
            return $this->renderError('你不是经销商!');
        }
        $list = $this->DerMermModer->getSonDer($user_id,$line,$perform_id);
        return $this->renderSuccess(compact('list'));
    }

    //经销商单成员消费明细
    public function orderDetail(){
        $page = input('page',1,'int');
        $line = input('line',10,'int');
        $income_id = input('income_id',0,'int');
        $user_id = input('user_id',0,'int');
        if(!$this->isRegistDer($user_id)){
            return $this->renderError('你不是经销商!');
        }
        $settleTime = $this->IncomeModel->where(['id'=>$income_id])->value('create_time');
        $saleList = $this->OrderModel->getTlist($settleTime,$user_id,$line,$page);
        return $this->renderSuccess(compact('saleList'),'经销商单成员消费明细获取成功');
    }

    //获取经销商信息(自己)
    public function getder(){
        $user_id = $this->userArr['user_id'];
        if(!$this->isRegistDer($user_id)){
            return $this->renderError('你不是经销商!');
        }
        $derinfo = $this->DerMermModer->getDerInfo($user_id);
        if(empty($derinfo)){
            return $this->renderError('获取经销商信息失败');
        }
        return $this->renderSuccess(compact('derinfo'),'经销商信息获取成功');
    }

    //根据经销商user_id查询经销商消费记录
    public function searchOrder(){
        $params = $this->postForm();
        $line = $params['line'] ?? 15;
        $line2 = $params['line2'] ?? 15;
        $perform_id = $params['perform_id'] ?? 0;
        $page = $params['page'] ?? 1;

        $addValidate = new \app\api\validate\wine\Dealer();
        if(!$addValidate->scene('searchOrder')->check($params)){
            return $this->renderError($addValidate->getError());
        }
        $user_id = $this->userArr['user_id'];
        if(!$this->isRegistDer($user_id)){
            return $this->renderError('你不是经销商!');
        }
        $income_id = $params['income_id'];

        //自己
        $myList = $this->getDList(0,$perform_id,$line,$line2,$income_id,$page);
        $otherList = $this->getDList($user_id,$perform_id,$line,$line2,$income_id,$page);
        $list = [
            'my' => $myList,
            'other' => $otherList
        ];
        return $this->renderSuccess(compact('list'),'经销商团队消费记录获取成功');
    }

    private function getDList($user_id,$perform_id,$line,$line2,$income_id,$page){
        $list = $this->DerMermModer->getSonDer2($user_id,$perform_id,$line);
        foreach ($list as $i=>$item){
            $user_id = $item['user_id'];
            $settleTime = $this->IncomeModel->where(['id'=>$income_id])->value('create_time');
            $saleList = $this->OrderModel->getTlist($settleTime,$user_id,$line2,$page);
            $list[$i]['saleList'] = $saleList;
        }
        return $list;
    }

    //判断是否注册经销商
    private function isRegistDer($user_id){
        return $this->DerMermModer->isRegist($user_id);
    }

    /**
     * 图片上传接口
     * @param int $groupId 分组ID
     * @return array
     * @throws \think\Exception
     */
    public function image(int $groupId = 0)
    {
        // 实例化存储驱动
        $storage = new StorageDriver($this->config);
        // 设置上传文件的信息
        $storage->setUploadFile('iFile')
            ->setRootDirName((string)$this->getStoreId())
            ->setValidationScene('image')
            ->upload();
        // 执行文件上传
        if (!$storage->upload()) {
            return $this->renderError('图片上传失败：' . $storage->getError());
        }
        // 文件信息
        $fileInfo = $storage->getSaveFileInfo();
        // 添加文件库记录
        $model = new UploadFileModel;
        $model->add($fileInfo, FileTypeEnum::IMAGE, $groupId);
        // 图片上传成功
        return $this->renderSuccess(['fileInfo' => $model->toArray()], '图片上传成功');
    }
}
