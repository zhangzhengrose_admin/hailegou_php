<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\model\CashRequest;
use app\api\model\wine\Dermember;
use app\api\model\wine\Cash as CashModel;
use think\App;
use app\api\validate\wine\Cash as CashValidate;

//提现
class Cash extends Controller{

    private $userArr = [];
    private $cashModel;
    private $validate;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->userArr = $this->getLoginUser();
        $this->cashModel = new CashModel();
        $this->validate = new CashValidate();
    }

    //申请提现
    public function applycash(){
        $params = $this->postForm();
        if(!$this->validate->check($params)){
            return $this->renderError($this->validate->getError());
        }
        $user_id = $this->userArr['user_id'];
        $cashRequest = new CashRequest();
        $r = $cashRequest->add($user_id,$params['cash_r']);
        if($r){
            return $this->renderSuccess([],'申请提现成功');
        }
        return $this->renderError('申请提现失败');
    }

    //提现-执行提现动作
    /*public function index(){
        $params = $this->postForm();
        $user_id = $this->userArr['user_id'];
        $cash = $params['cash'];
        $derModel = new Dermember();
        $this->cashModel->transaction(function () use($derModel,$cash,$user_id){
            //提现事物
            $this->cashEvent();
            $derModel->cash($cash,$user_id);
            //提现记录
            $this->cashModel->cashRecord($cash,$user_id);
        });
        return $this->renderSuccess([],'提现成功');
    }
    //执行提现事物
    public function cashEvent(){}*/
    //提现明细
    public function record(){
        $page = input('page',1);
        $line = input('line',10);
        $user_id = $this->userArr['user_id'];
        $list = $this->cashModel->getList($user_id,$page,$line);
        return $this->renderSuccess(compact('list'));
    }
}
