<?php
namespace app\api\controller;

class Version extends Controller {
    public function index(){
        $versionModel = new \app\store\model\Version();
        $detail = $versionModel->getDetail($this->getStoreId());
        return $this->renderSuccess(compact('detail'));
    }
}
