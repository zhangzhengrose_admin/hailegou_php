<?php
declare (strict_types = 1);

namespace app\command;

use app\api\model\wine\Perform;
use app\command\model\Order;
use app\store\model\store\Dermember;
use app\store\model\store\Income;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\facade\Cache;

//计算业绩定时任务
class Wine extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('wine')
            ->addArgument('start', Argument::REQUIRED, '开始执行任务')
            ->setDescription('the perform command');
    }


    protected function execute(Input $input, Output $output)
    {
        $start = $input->getArgument('start');
        switch ($start){
            //团队业绩结算定时任务
            case 'start':
                $output->writeln('dermember表团队销售统计-定时任务开始执行');
                $this->start();
                $output->writeln('dermember表团队销售统计-定时任务执行结束');
                break;
            case 'order':
                if($this->order()){
                    $output->writeln('订单状态修改成功');
                }else{
                    $output->writeln('订单状态未修改');
                }
                break;
            default:
                $output->writeln('未定义指令');
                break;
        }
    }

    //定时任务执行的内容
    private function start(){
        $derModel = new Dermember();
        $performModel = new Perform();
        $incomeMode = new Income();
        $derModel->transaction(function () use($derModel,$performModel,$incomeMode){
            $jxsArr = $derModel->where(['is_delete'=>0])
                ->where('user_id','<>','0')
                ->with('user')
                ->select();
            foreach ($jxsArr as $value){
                $id = $value['id'];
//                $idCard = $value['idCard'];
//                $derModel->updateUserId($idCard);
                $user_id = $value['user_id'];
                $getPerform = $this->getPerform($user_id);
                $expend_money = $value['pay_money'];
                //团队人数 包含自己所以+1
                $pcount = $value['pcount'] + 1;
                $total_expend_money = $getPerform + $expend_money;
                // 系统是否设置了经销商级别
                $perform_id_set = $value['perform_id_set'];
                //获取级别
                $levelData = $performModel->getLevel($total_expend_money,$pcount,$perform_id_set);
                //收益100%
                $award = $levelData['award']/100;
                //经销商配置表id
                $perform_id = $levelData['id'];
                //个人提现总金额
                $cash = $derModel->where(['id'=>$id])->value('cash');
                //个人总收益
                $revenue = $total_expend_money*$award;
                //总余额
                $total_balance = $revenue - $cash;
                //经销商团队收入增量结算记录
                $incomeMode->writeBill($revenue,$user_id);
                //经销商团队结算
                $derModel->where(['id'=>$id])->save([
                    'total_expend_money'=>$total_expend_money,
                    'revenue' => $revenue,
                    'balance' => $total_balance,
                    'perform_id' => $perform_id
                ]);
            }
        });
    }

    //查找子经销商业绩
    private function getPerform($pid){
        $derModel = new Dermember();
        $r = $derModel->where(['pid'=>$pid])->with('user')->select();
        $total_expend_money = 0;
        foreach ($r as $value){
            $expend_money = $value['pay_money'];
            $total_expend_money += $expend_money;
        }
        //经销商个数
        $pcount = count($r);
        //统计团队收益
        $derModel->where(['user_id'=>$pid])->save([
            'total_expend_money'=>$total_expend_money,
            'pcount'=>$pcount
        ]);
        return $total_expend_money;
    }
    // 订单过期处理
    private function order(){
        return Order::orderRun();
    }
}
