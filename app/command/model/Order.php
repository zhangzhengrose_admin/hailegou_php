<?php
namespace app\command\model;

use app\common\model\BaseModel;

class Order extends BaseModel{
    protected $pk = 'order_id';

    // 4小时订单过期
    public static function orderRun(){
        $beforeTime = time()-14400; // 4小时过期
        return self::where(['order_status'=>10,'pay_status'=>'10'])
            ->where('create_time','<',$beforeTime)
            ->save(['order_status'=>20]);
    }
}
