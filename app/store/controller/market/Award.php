<?php
namespace app\store\controller\market;

use app\store\controller\Controller;
use \app\store\model\store\Dermember as DermemberModel;
use think\App;

class Award extends Controller{
    private $dermemberModel;
    public function __construct(App $app)
    {
        $this->dermemberModel = new DermemberModel();
        parent::__construct($app);
    }
    //经销商业绩奖励
    public function detail(){
        $id = input('id',0,'int');
        $detail = $this->dermemberModel->getAward($id);
        return $this->renderSuccess(compact('detail'),'成功获取经销商业绩奖励');
    }
}
