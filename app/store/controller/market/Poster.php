<?php
namespace app\store\controller\market;

use app\store\controller\Controller;
use app\store\model\Poster as PosterModel;
use think\App;

class Poster extends Controller{
    private $posterModel;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->posterModel = new PosterModel();
    }

    public function detail(){
        $store_id = self::getStoreId();
        $values = $this->posterModel->detail($store_id);
        if($values){
            return $this->renderSuccess(compact('values'));
        }else{
            return $this->renderError('获取信息失败');
        }
    }

    public function edit(){
        $params = json_decode(input('form/s','',null),true);
        $r = $this->posterModel->setData($params);
        if($r){
            return $this->renderSuccess(['url'=>$r],'更新成功');
        }else{
            return $this->renderError('更新失败');
        }
    }
}
