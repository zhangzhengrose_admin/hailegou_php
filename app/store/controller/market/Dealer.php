<?php
namespace app\store\controller\market;

use app\store\controller\Controller;
use app\store\model\store\Dealer as DealerModel;

class Dealer extends Controller{
    //获取第一条信息
    public function detail(){
        $model = new DealerModel();
        $values = $model->detail();
        if($values){
            return $this->renderSuccess(compact('values'));
        }else{
            return $this->renderError('获取信息失败');
        }
    }
    //酒类业绩计算表编辑
    public function edit(){
        $model = new DealerModel();
        $r = $model->edit($this->postForm());
        if($r){
            return $this->renderSuccess('更细成功');
        }else{
            return $this->renderError('更新失败');
        }
    }
}
