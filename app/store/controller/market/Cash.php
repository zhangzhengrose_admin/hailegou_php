<?php
namespace app\store\controller\market;

use app\store\controller\Controller;
use app\store\model\market\CashRequest;
use app\store\model\market\Cash as CashModel;
use think\App;

class Cash extends Controller{
    private $cashRequestModel;
    private $cashModel;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->cashRequestModel = new CashRequest();
        $this->cashModel = new CashModel();
    }

    //获取申请记录
    public function getApplylist(){
        $params = $this->request->param();
        $list = $this->cashRequestModel->getList($params);
        return $this->renderSuccess(compact('list'));
    }

    //提现
    public function cash(){
        $params = $this->request->param();
        if($this->cashRequestModel->setStatus($params['id'],$params['status'])){
            return $this->renderSuccess('成功提现!');
        }
        return $this->renderSuccess('提现失败!');
    }

    //驳回
    public function reject(){
        $params = $this->request->param();
        $r = $this->cashRequestModel->setStatus($params['id'],$params['status']);
        if($r){
            return $this->renderSuccess('执行成功!');
        }else{
            return $this->renderError('执行失败');
        }
    }
}
