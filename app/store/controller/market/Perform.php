<?php
namespace app\store\controller\market;

use app\store\controller\Controller;
use app\store\model\store\Perform as PerformModel;

class Perform extends Controller{
    //获取第一条信息
    public function detail(){
        $model = new PerformModel();
        $values = $model->detail($this->request->param());
        if($values){
            return $this->renderSuccess(compact('values'));
        }else{
            return $this->renderError('获取信息失败');
        }
    }
    private function postForm2(){
        $json = input('form','',null);
        return json_decode($json,true);
    }
    //添加酒业业绩计算表
    public function add(){
        $model = new PerformModel();
        $data = $this->postForm2();
        if($model->add($data)){
            return $this->renderSuccess('添加成功');
        }
        return $this->renderError('添加失败');
    }
    //酒类业绩计算表编辑
    public function edit(){
        $model = new PerformModel();
        $r = $model->edit($this->postForm());
        if($r){
            return $this->renderSuccess('更新成功');
        }else{
            return $this->renderError('更新失败');
        }
    }
    //获取酒类业绩计算列表
    public function getList(){
        $model = new PerformModel();
        $param = $this->request->param();
        $list = $model->lists($param);
        return $this->renderSuccess(compact('list'));
    }
    //删除
    public function del(){
        $model = new PerformModel();
        if($model->del($this->request->param())){
            return $this->renderSuccess('删除成功');
        }
        return $this->renderError('删除失败');
    }
    //设置商品状态
    public function setStatus(){
        $model = new PerformModel();
        if($model->setStatus($this->request->param())){
            return $this->renderSuccess('状态设置成功');
        }
        return $this->renderError('状态设置失败');
    }
}
