<?php
namespace app\store\controller\client;

use app\store\controller\Controller;
use app\store\model\Version;

class App extends Controller{
    private $versionModel;
    public function __construct(\think\App $app)
    {
        parent::__construct($app);
        $this->versionModel = new Version();
    }

    public function detail(){
        $detail = $this->versionModel->getDetail(getStoreId());
        return $this->renderSuccess(compact('detail'));
    }

    public function setting(){
        $params = $this->postForm();
        if($this->versionModel->setData($params,getStoreId())){
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError($this->versionModel->getError() ?: '更新失败');
    }
}
