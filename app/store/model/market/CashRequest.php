<?php
namespace app\store\model\market;

use app\common\model\User;
use think\Model;

class CashRequest extends Model{
    protected $autoWriteTimestamp = true;

    //经销商相关用户信息
    public function user(){
        return $this->hasOne(User::class,'user_id','user_id')
            ->bind(['nick_name','mobile','gender']);
    }
    //设置提现状态
    public function setStatus($id,$status){
        return $this->where(['id'=>$id])->save(['status'=>$status]);
    }

    // 提现申请记录
    public function getList($params){
        $model = self::where(['status'=>$params['status']])->with('user');

        isset($params['mobile']) &&
        !empty($params['mobile']) &&
        $model = self::hasWhere('user',['mobile'=>$params['mobile']])->where(['status'=>$params['status']])->with('user');

        $list = $model->order('create_time desc')->paginate($params['limit']);
        $userModel = new User();
        $dataArr = $list->toArray();
        foreach ($dataArr['data'] as $index => $arr){
            $user_id = $arr['user_id'];
            $userArr = $userModel->where(['user_id'=>$user_id])->with('avatar')->find();
            $arr['avatar_url'] = $userArr['avatar_url']==null?'':$userArr['avatar_url'];
            $dataArr['data'][$index] = $arr;
        }
        return $dataArr;
    }
}
