<?php
namespace app\store\model\store;

use app\common\model\BaseModel;

class Perform extends BaseModel{
    // 定义表名
    protected $name = 'perform';

    // 定义主键
    protected $pk = 'id';

    //百分比数值加上百分号
    public function getAwardAttr($value){
        return $value.'%';
    }

    //百分比不带百分号
    public function getAwardnoAttr($value){
        return $value;
    }

    public function add(array $data){
        return $this->save([
            'store_id' => self::$storeId,
            'name' => $data['name'],
            'award' => $data['award'],
            'pcount' => $data['pcount'],
            'performance' => $data['performance'],
            'direct_award' => $data['direct_award'],
            'des' => $data['des']
        ]);
    }

    //默认数据
    public function enumData(){
        return [
            'store_id' => self::$storeId,
            'name' => '初级代理',
            'award' => 15,
            'pcount' => 1,//自身算1人
            'performance' => 2000,
            'direct_award' => 1,
            'des' => '酒类业绩呈现'
        ];
    }

    //获取第一条信息
    public function detail($data){
        $id = $data['id'];
        $r = (new Perform)->where(['store_id'=>self::$storeId,'id'=>$id])->find();
        if($r->isEmpty()){
            $data = $this->enumData();
            $this->add($data);
            return $data;
        }else{
            return $r;
        }
    }

    //更新第一条信息
    public function edit($data){
        $r = self::where(['id' => $data['id']])->find();
        $award = str_replace('%','',$data['award']);
        $pcount = $data['pcount'];
        //团队人数必须1人以上
        if($pcount<1){
            return false;
        }
        return $r->save([
            'store_id' => self::$storeId,
            'name' => $data['name'],
            'award' => $award,
            'pcount' => $pcount,
            'performance' => $data['performance'],
            'direct_award' => $data['direct_award'],
            'des' => $data['des']
        ]);
    }
    //获取数据列
    public function lists($where){
        $limit = $where['limit'];
        $query = self::where(['status'=>$where['status']]);
        if(isset($where['name'])){
            return $query->whereLike('name',"%{$where['name']}%")
                ->paginate($limit);
        }
        return $query->paginate($limit);
    }
    //删除数据
    public function del($data)
    {
        $id = $data['id'];
        return $this->where(['id'=>$id])->delete();
    }
    //设置商品状态
    public function setStatus($param){
        $status = $param['status'];
        $status = $status==1?0:1;
        return $this->where(['id'=>$param['id']])->save(['status'=>$status]);
    }
}
