<?php
namespace app\store\model\store;

use app\common\model\BaseModel;

class Dealer extends BaseModel{
    // 定义表名
    protected $name = 'dealer';

    // 定义主键
    protected $pk = 'id';

    private function add(array $data){
        $r = $this->save([
            'store_id' => self::$storeId,
            'per_wine' => $data['per_wine'],
            'dealers' => $data['dealers'],
            'car_award' => $data['car_award'],
            'car_wine' => $data['car_wine'],
            'before' => $data['before'],
            'total' => $data['total'],
            'date_end' => $data['date_end'],
            'total_q' => $data['total_q'],
            'profit_p' => $data['profit_p'],
        ]);
        if($r){
            return true;
        }
        return false;
    }

    //默认数据
    public function enumData(){
        return [
            'store_id' => self::$storeId,
            'per_wine' => '8', //8%酒体
            'dealers' => '7', //7名经销商
            'car_award' => 150000, //车奖15万
            'car_wine' => 150000, //酒体奖15万
            'before' => 50,//前50位
            'total' => 1000000,//总业绩100万达标
            'date_end' => 20210930,//活动截止日期
            'total_q' => 200000,//每季度业绩20万
            'profit_p' => 8,//每季度分红8%
        ];
    }

    //获取第一条信息
    public function detail(){
        $r = (new Dealer)->where(['store_id'=>self::$storeId])->limit(0,1)->find();
        if($r->isEmpty()){
            $data = $this->enumData();
            $this->add($data);
            return $data;
        }else{
            return $r;
        }
    }

    //更新第一条信息
    public function edit($data){
        $id = (new Perform)->where(['store_id'=>self::$storeId])->limit(0,1)->value('id');
        if(!empty($id)){
            return $this->where(['id' => $id])
                ->save(
                    [
                        'store_id' => self::$storeId,
                        'per_wine' => $data['per_wine'],
                        'dealers' => $data['dealers'],
                        'car_award' => $data['car_award'],
                        'car_wine' => $data['car_wine'],
                        'before' => $data['before'],
                        'total' => $data['total'],
                        'date_end' => $data['date_end'],
                        'total_q' => $data['total_q'],
                        'profit_p' => $data['profit_p'],
                        'update_time'=>time()
                    ]
                );
        }else{
            return $this->add($data);
        }
    }
}
