<?php
namespace app\store\model\store;

use app\common\model\BaseModel;

class Income extends BaseModel {
    // 定义表名
    protected $name = 'income';

    // 定义主键
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
    //写入直推收益记录
    public function directRecord($revenue,$user_id){
        $data = [
            'user_id'=> $user_id,
            'store_id'=> getStoreId(),
            'revenue' => $revenue,
            'type' => 2
        ];
        return $this->save($data);

    }
    //经销商团队收入增量结算记录
    public function writeBill($revenue,$user_id){
        //计算增量
        $near_revenue = $this->where(['user_id'=>$user_id])
            ->sum('revenue');
        $delta_revenue = $revenue - $near_revenue;
        $delta_revenue = floor($delta_revenue*100)/100;
        $data = [
            'user_id'=> $user_id,
            'store_id'=> getStoreId(),
            'revenue' => $delta_revenue,
            'type' => 1
        ];
        if($delta_revenue>0){
            $this->save($data);
        }
    }
    //经销商收益记录
    public function getList($user_id,$page,$line,$type=1){
        return $this->where(['user_id'=>$user_id,'type'=>$type])
                ->page($page,$line)
                ->select();
    }
}
