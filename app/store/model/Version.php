<?php
namespace app\store\model;

use app\common\model\BaseModel;

class Version extends BaseModel{
    protected $autoWriteTimestamp = true;

    public function setData($params,$store_id){
        $r = self::where(['store_id'=>$store_id])->findOrEmpty();
        if($r->isEmpty()){
            return $this->save([
                'version' => $params['version'],
                'url' => $params['url'],
                'store_id' => $store_id,
                'remark' => $params['remark']
            ]);
        }else{
            return $this->where(['store_id'=>$store_id])
                ->save([
                    'version' => $params['version'],
                    'url' => $params['url'],
                    'remark' => $params['remark']
                ]);
        }
    }

    public function getDetail($store_id){
        return $this->where(['store_id'=>$store_id])
            ->field('url,version,remark')
            ->find();
    }
}
