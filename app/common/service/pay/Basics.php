<?php
// +----------------------------------------------------------------------
// | 萤火商城系统 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 https://www.yiovo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 萤火科技 <admin@yiovo.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\common\service\pay;

use app\common\model\store\Setting as SettingModel;
use app\common\library\pay\Driver as PayDriver;
use app\common\service\BaseService;

/**
 * 消息通知服务[基类]
 * Class Basics
 * @package app\common\service\message
 */
abstract class Basics extends BaseService
{
    // 参数列表
    protected $param = [];
    // 商城ID
    protected $storeId;
    // 错误信息
    protected $error = '';
    protected $response = ''; //返回json格式数据
    protected $httpCode = 500;//服务器响应状态
    protected $code = true;
    // 支付设置
    private $smsConfig;

    private $PayDriver;




    /**
     * 构造方法
     * Basics constructor.
     * @param int $storeId
     */
    public function __construct(int $storeId)
    {
        parent::__construct();
        $this->storeId = $storeId;
        $this->smsConfig = SettingModel::getItem('pay', $this->storeId);
        $this->PayDriver = new PayDriver($this->smsConfig);
    }

    /*//子商户注册接口
    abstract public function income(array $param);

    //修改商户信息
    abstract public function modifys(array $param);

    //绑卡申请
    abstract public function bindCardApply(array $param);

    //绑卡确认
    abstract public function bindCardConfirm(array $param);

    //发起支付
    abstract public function pay(array $param);

    ////代付
    abstract public function repay(array $param);

    //订单查询
    abstract public function orderQuery(array $param);*/

    //子商户注册接口
    protected function incomeSubmit(string $sceneValue, array $templateParams, array $sceneConfig = [])
    {
        $this->PayDriver->income($sceneValue, $templateParams, $sceneConfig);
        return $this->setAttr();
    }

    //修改商户信息
    protected function modifysSubmit(string $sceneValue, array $templateParams, array $sceneConfig = [])
    {
        $this->PayDriver->modifys($sceneValue, $templateParams, $sceneConfig);
        return $this->setAttr();
    }

    //绑卡申请
    protected function bindCardApplySubmit(string $sceneValue, array $templateParams, array $sceneConfig = [])
    {
        $this->PayDriver->bindCardApply($sceneValue, $templateParams, $sceneConfig);
        return $this->setAttr();
    }
    //绑卡确认
    protected function bindCardConfirmSubmit(string $sceneValue, array $templateParams, array $sceneConfig = [])
    {
        $this->PayDriver->bindCardConfirm($sceneValue, $templateParams, $sceneConfig);
        return $this->setAttr();
    }

    //提交支付请求
    protected function paySubmit(string $sceneValue, array $templateParams, array $sceneConfig = [])
    {
        $this->PayDriver->pay($sceneValue, $templateParams, $sceneConfig);
        return $this->setAttr();
    }

    //提交消费请求
    protected function consumeSubmit(string $sceneValue, array $templateParams, array $sceneConfig = [])
    {
        $this->PayDriver->consume($sceneValue, $templateParams, $sceneConfig);
        return $this->setAttr();
    }

    //提交代付请求
    protected function repaySubmit(string $sceneValue, array $templateParams, array $sceneConfig = [])
    {
        $this->PayDriver->repay($sceneValue, $templateParams, $sceneConfig);
        return $this->setAttr();
    }

    //提交订单查询请求
    protected function orderQuerySubmit(string $sceneValue, array $templateParams, array $sceneConfig = [])
    {
        $this->PayDriver->orderQuery($sceneValue, $templateParams, $sceneConfig);
        return $this->setAttr();
    }

    /**
     * 字符串截取前20字符
     * [用于兼容thing数据类型]
     * @param string $content
     * @param int $length
     * @return bool|string
     */
    protected function getSubstr(string $content, int $length = 20)
    {
        return str_substr($content, $length);
    }

    /**
     * 获取错误信息
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }
    //获取请求
    public function getResponse(){
        return $this->response;
    }
    // 获取服务器响应状态
    public function getHttpCode(){
        return $this->httpCode;
    }

    public function getCode(){
        return $this->code;
    }

    protected function setParams(array $params){
        $this->PayDriver->setParams($params);
    }

    //设置属性
    private function setAttr(){
        $this->error = $this->PayDriver->error;
        $this->response = $this->PayDriver->response;
        $this->httpCode = $this->PayDriver->httpCode;
        $this->code = $this->PayDriver->code;
        return $this;
    }

}
