<?php
// +----------------------------------------------------------------------
// | 萤火商城系统 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 https://www.yiovo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 萤火科技 <admin@yiovo.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\common\service\pay\order;

use app\common\service\pay\Basics;
use app\common\model\store\Setting as SettingModel;
use app\common\enum\setting\sms\Scene as SettingSmsScene;

/**
 * 子商户注册服务 [订单支付成功]
 * Class Payment
 * @package app\common\service\message\order
 */
class Income extends Basics
{
    /**
     * 参数列表
     * @var array
     */
    protected $param = [
        'order' => []
    ];


    /**
     * 订单页面链接
     * @var array
     */
    private $pageUrl = 'pages/order/detail';

    //发起子商户注册
    public function exec(array $param)
    {
        // 记录参数
        $this->param = $param;
        // 短信通知商家
        return $this->onIncome();
    }

    /**
     * 发起子商户注册接口
     * @return Income
     */
    private function onIncome()
    {
        $orderId = $this->param['orderId'];
        //设置参数
        $this->setParams($this->param);
        return  $this->incomeSubmit(SettingSmsScene::ORDER_PAY, ['order_no' => $orderId]);
    }

    /**
     * 格式化商品名称
     * @param $goodsData
     * @return string
     */
    private function getFormatGoodsName($goodsData)
    {
        return $this->getSubstr($goodsData[0]['goods_name']);
    }
}
