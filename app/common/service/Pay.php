<?php
// +----------------------------------------------------------------------
// | 萤火商城系统 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 https://www.yiovo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 萤火科技 <admin@yiovo.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\common\service;

/**
 * 支付通道服务
 * Class Message
 * @package app\common\service
 */
class Pay extends BaseService
{
    /**
     * 场景列表
     * [场景名称] => [场景类]
     * @var array
     */
    private static $sceneList = [
        // 子商户注册接口
        'order.income' => \app\common\service\pay\order\Income::class,
        // 修改商户信息
        'order.modifys' => \app\common\service\pay\order\Modifys::class,
        // 绑卡申请
        'order.bindCardApply' => \app\common\service\pay\order\BindCardApply::class,
        // 绑卡确认
        'order.bindCardConfirm' => \app\common\service\pay\order\BindCardConfirm::class,
        // 支付
        'order.pay' => \app\common\service\pay\order\Pay::class,
        // 支付
        'order.consume' => \app\common\service\pay\order\Consume::class,
        // 代付
        'order.rePay' => \app\common\service\pay\order\Repay::class,
        // 订单查询
        'order.orderQuery' => \app\common\service\pay\order\OrderQuery::class,
    ];

    /**
     * SM支付通道
     * @param string $sceneName
     * @param array $param
     * @param int $storeId
     * @return false|mixed 这里返回对象
     * {#78
    +"code": 200
    +"message": "请求成功"
    +"sign": "1ab3fbf8e1e66d13cf04cc00b915dc49"
    +"data": {#79
    +"merchId": "1200202431"
    }
    }
     */
    public static function sendSM(string $sceneName, array $param, int $storeId)
    {
        if (!isset(self::$sceneList[$sceneName])) return false;
        $class = self::$sceneList[$sceneName];
        return (new $class($storeId))->exec($param);
    }

}
