<?php
namespace app\common\library\pay\engine;

use app\common\library\pay\package\smpay\SmPay as SmpayPackage;

/**
 * 跨境支付通道
 * Class Smpay
 * @author Zhangzheng
 * @package app\common\library\pay\engine
 */
class Smpay extends SmpayPackage {
    private $config;
    public $params;

    /**
     * 构造方法
     * Qiniu constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->private_key = $config['oemsys_privatekey'];
        $this->branchCode = $config['oemsys_branchCode'];
    }

    /**
     * 设置参数
     * @param $params
     */
    public function setParams($params){
        $this->params = $params;
    }

    /**
     * 子商户注册接口
     * @param array $sceneConfig
     * @param array $templateParams
     * @return Smpay
     */
    public function income(array $sceneConfig, array $templateParams)
    {
        $url =$this->config['oemsys_url'].'register/income';
        $params = [
            'channelId' => $this->config['channelId'],
            'orderId'   => $this->params['orderId'],
            'merchName' => $this->config['merName'],
            'idNo' => $this->params['idNo'],
            'idName' => $this->params['idName'],
            'feeRate' => $this->params['feeRate'],
            'extendFee' => $this->params['extendFee'],
            'settCardNo' => $this->params['settCardNo'],
            'settPhone' => $this->params['settPhone'],
            'address' => $this->params['address'],
            'cityName' => $this->params['cityName'],
            'idCardFront' => $this->params['idCardFront'],
            'idCardBack' => $this->params['idCardBack']
        ];
        return $this->base($url,$params);
    }

    /**
     * 修改商户信息
     * @param array $sceneConfig
     * @param array $templateParams
     * @return Smpay
     */
    public function modifys(array $sceneConfig, array $templateParams)
    {
        $url =$this->config['oemsys_url'].'register/modify';
        $params = [
            'orderId'   => $this->params['orderId'],
            'merchId' => $this->params['merchId'],
            'type' => $this->params['type'],
            'idNo' => $this->params['idNo'],
            'idName' => $this->params['idName'],
            'settCardNo' => $this->params['settCardNo'],
            'settPhone' => $this->params['settPhone'],
            'feeRate' => $this->params['feeRate'],
            'extendFee' => $this->params['extendFee'],
            'idCardFront' => $this->params['idCardFront'],
            'idCardBack' => $this->params['idCardBack']
        ];
        return $this->base($url,$params);
    }

    /**
     * 绑卡申请
     * @param array $sceneConfig
     * @param array $templateParams
     * @return Smpay
     */
    public function bindCardApply(array $sceneConfig, array $templateParams)
    {
        $url =$this->config['oemsys_url'].'trade/bindCardApply';
        $params = [
            'orderId'   => $this->params['orderId'],
            'transType' => $this->params['transType'],
            'merchId' => $this->params['merchId'],
            'idNo' => $this->params['idNo'],
            'idName' => $this->params['idName'],
            'cardId' => $this->params['cardId'],
            'bankPhone' => $this->params['bankPhone'],
            'cvv' => $this->params['cvv'],
            'expireDate' => $this->params['expireDate'],
            'pageUrl' => $this->params['pageUrl']
        ];
        return $this->base($url,$params);
    }

    /**
     * 绑卡确认
     * @param array $sceneConfig
     * @param array $templateParams
     * @return Smpay
     */
    public function bindCardConfirm(array $sceneConfig, array $templateParams)
    {
        $url =$this->config['oemsys_url'].'trade/bindCardConfirm';
        $params = [
            'orderId'   => $this->params['orderId'],
            'oriOrderId' => $this->params['oriOrderId'],
            'merchId' => $this->params['merchId'],
            'smsCode' => $this->params['smsCode']
        ];
        return $this->base($url,$params);
    }

    /**
     * 发起支付
     * @param array $sceneConfig 场景配置
     * @param array $templateParams 短信模板参数
     * @return Smpay
     */
    public function pay(array $sceneConfig, array $templateParams){
        $url =$this->config['oemsys_url'].'trade/consume';
        if(empty($this->params['currency'])){
            $this->code = false;
            $this->msg = '缺少currency参数,请检查!';
            $this->response = [];
            $this->httpCode = 500;
            return $this;
        }
        //选择支付币种 USD
        $currency = empty($this->params['currency'])?'USD':$this->params['currency'];
        $merchId = $this->config['merch'][$currency]['merchId'];

        // 1元人民币汇率
        $rate = $this->config['merch'][$currency]['rate'];
        //订单金额从人民币换算成对应币种
        $transAmt = $this->params['transAmt']*$rate;
        //丢掉小数点
        $transAmt = floor($transAmt);
        //小于1则用1代替
        if($transAmt<1){
            $transAmt = 1;
        }

        $params = [
            'orderId'   => $this->params['orderId'],
            'transAmt' => $transAmt,
            'merchId' => $merchId,
            'transType' => $this->params['transType'],
            'cardId' => $this->params['cardId'],
            'userIp' => $this->params['userIp'],
            'add1' => $this->params['add1'],
            'notifyUrl' => $this->getNotifyUrl(),
        ];
        return $this->base($url,$params);
    }

    /**
     * 发起消费
     * @param array $sceneConfig
     * @param array $templateParams
     * @return Smpay
     */
    public function consume(array $sceneConfig, array $templateParams){
        $url = $this->config['oemsys_url'].'trade/consume';
        $params = [
            'orderId'   => $this->params['orderId'],
            'transAmt' => $this->params['transAmt'],
            'merchId' => $this->params['merchId'],
            'transType' => $this->params['transType'],
            'idNo' => $this->params['idNo'],
            'idName' => $this->params['idName'],
            'cardId' => $this->params['cardId'],
            'bankPhone' => $this->params['bankPhone'],
            'cvv' => $this->params['cvv'],
            'expireDate' => $this->params['expireDate'],
            'cityName' => $this->params['cityName'],
            'userIp' => $this->params['userIp'],
            'deviceId' => $this->params['deviceId'],
            'notifyUrl' => $this->getNotifyUrl(),
        ];
        return $this->base($url,$params);
    }

    /**
     * 代付
     * @param array $sceneConfig
     * @param array $templateParams
     * @return Smpay
     */
    public function repay(array $sceneConfig, array $templateParams)
    {
        $url =$this->config['oemsys_url'].'trade/withDraw';
        $params = [
            'orderId'   => $this->params['orderId'],
            'transAmt' => $this->params['transAmt'],
            'merchId' => $this->params['merchId'],
            'transType' => $this->params['transType'],
            'idNo' => $this->params['idNo'],
            'idName' => $this->params['idName'],
            'cardId' => $this->params['cardId'],
            'bankPhone' => $this->params['bankPhone'],
            'cvv' => $this->params['cvv'],
            'expireDate' => $this->params['expireDate'],
            'settType' => 'D0',
            'notifyUrl' => $this->getNotifyUrl(),
        ];
        return $this->base($url,$params);
    }

    /**
     * 订单查询
     * @param string $orderId
     * @return Smpay
     */
    public function orderQuery(array $sceneConfig, array $templateParams)
    {
        $url =$this->config['oemsys_url'].'trade/orderQuery';
        $params = [
            'orderId'   => $this->params['orderId']
        ];
        return $this->base($url,$params);
    }

    //获取异步请求地址
    private function getNotifyUrl(){
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        if(isset($_SERVER['HTTP_HOST'])){
            return $http_type . $_SERVER['HTTP_HOST'] .'/'.$this->config['notify'];
        }
        return $http_type .$this->config['domain'].'/'.$this->config['notify'];
    }
}
