<?php
// +----------------------------------------------------------------------
// | 萤火商城系统 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 https://www.yiovo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 萤火科技 <admin@yiovo.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\common\library\authenticate;

use think\Exception;

/**
 * 实名认证模块驱动
 * Class driver
 * @package app\common\library\pay
 */
class Driver
{
    private $config;    // 配置信息
    private $engine;    // 当前短信服务引擎类
    private $engineName;    // 当前短信引擎名称

    public $error = '';    // 错误信息
    public $response = ''; //返回json格式数据
    public $httpCode = 500;//服务器响应状态
    public $code = true; //服务器响应状态 布尔
    public $msg = '';
    private $params;

    /**
     * 构造方法
     * Driver constructor.
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $config)
    {
        // 配置信息
        $this->config = $config;
        // 当前引擎名称
        $this->engineName = $this->config['default'];
        // 实例化当前存储引擎
        $this->engine = $this->getEngineClass();
    }

    /**
     * 设置参数
     * @param $params
     */
    public function setParams(array $params){
        $this->engine->params = $params;
    }

    //子商户注册接口
    public function auth(){
        $this->engine->auth();
        return $this->setAttr();
    }

    public function auth4(){
        $this->engine->auth4();
        return $this->setAttr();
    }

    //设置属性
    private function setAttr(){
        $this->error = $this->engine->error;
        $this->msg = $this->engine->msg;
        $this->response = $this->engine->response;
        $this->httpCode = $this->engine->httpCode;
        $this->code = $this->engine->code;
        return $this;
    }

    /**
     * 获取错误信息
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }
    //获取请求
    public function getResponse(){
        return $this->response;
    }
    // 获取服务器响应状态
    public function getHttpcode(){
        return $this->httpCode;
    }

    public function getCode(){
        return $this->code;
    }

    public function getMsg(){
        return $this->msg;
    }

    /**
     * 获取当前的存储引擎
     * @return mixed
     * @throws Exception
     */
    private function getEngineClass()
    {
        $classSpace = __NAMESPACE__ . '\\engine\\' . ucfirst($this->engineName);
        if (!class_exists($classSpace)) {
            throwError('未找到存储引擎类: ' . $this->engineName);
        }
        return new $classSpace($this->config['engine'][$this->engineName]);
    }

}
