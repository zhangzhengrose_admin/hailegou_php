<?php











namespace Composer;

use Composer\Autoload\ClassLoader;
use Composer\Semver\VersionParser;








class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '74590d73961429e48439e11219bb2817ed2111ea',
    'name' => 'topthink/think',
    'dev' => true,
  ),
  'versions' => 
  array (
    'adbario/php-dot-notation' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eee4fc81296531e6aafba4c2bbccfc5adab1676e',
      'dev-requirement' => false,
    ),
    'alibabacloud/tea' => 
    array (
      'pretty_version' => '3.1.22',
      'version' => '3.1.22.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f9c9b2c927253a1c23a5381cc655e41311be7f65',
      'dev-requirement' => false,
    ),
    'alibabacloud/tea-fileform' => 
    array (
      'pretty_version' => '0.3.4',
      'version' => '0.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '4bf0c75a045c8115aa8cb1a394bd08d8bb833181',
      'dev-requirement' => false,
    ),
    'alipaysdk/easysdk' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a1cfa83c7e140bded957498ea072c77611e6480',
      'dev-requirement' => false,
    ),
    'aliyuncs/oss-sdk-php' => 
    array (
      'pretty_version' => 'v2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '053d7ba9e798e4c09b9c5c1edab153d25ea9643a',
      'dev-requirement' => false,
    ),
    'danielstjules/stringy' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df24ab62d2d8213bbbe88cc36fc35a4503b4bd7e',
      'dev-requirement' => false,
    ),
    'edward1108/edward-captcha' => 
    array (
      'pretty_version' => '1.1',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '426ceee34507c30d4b21e0dd349b571371aef700',
      'dev-requirement' => false,
    ),
    'guzzle/batch' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/cache' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/common' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/guzzle' => 
    array (
      'pretty_version' => 'v3.9.3',
      'version' => '3.9.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0645b70d953bc1c067bbc8d5bc53194706b628d9',
      'dev-requirement' => false,
    ),
    'guzzle/http' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/inflection' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/iterator' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/log' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/parser' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-async' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-backoff' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-cache' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-cookie' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-curlauth' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-error-response' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-history' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-log' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-md5' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-mock' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/plugin-oauth' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/service' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzle/stream' => 
    array (
      'dev-requirement' => false,
      'replaced' => 
      array (
        0 => 'v3.9.3',
      ),
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.3.0',
      'version' => '7.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7008573787b430c1c1f650e3722d9bba59967628',
      'dev-requirement' => false,
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e7d04f1f6450fef59366c399cfad4b9383aa30d',
      'dev-requirement' => false,
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1dc8d9cba3897165e16d12bb13d813afb1eb3fe7',
      'dev-requirement' => false,
    ),
    'kosinix/grafika' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '211f61fc334b8b36616b23e8af7c5727971d96ee',
      'dev-requirement' => false,
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.0.69',
      'version' => '1.0.69.0',
      'aliases' => 
      array (
      ),
      'reference' => '7106f78428a344bc4f643c233a94e48795f10967',
      'dev-requirement' => false,
    ),
    'league/flysystem-cached-adapter' => 
    array (
      'pretty_version' => '1.0.9',
      'version' => '1.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '08ef74e9be88100807a3b92cc9048a312bf01d6f',
      'dev-requirement' => false,
    ),
    'lvht/geohash' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbba3e1b487f0ec2e5e666c1bc9d1d4277990a29',
      'dev-requirement' => false,
    ),
    'mtdowling/jmespath.php' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
      'dev-requirement' => false,
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.7.6',
      'version' => '1.7.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f36467c7a87e20fbdc51e524fd8f9d1de80187c',
      'dev-requirement' => false,
    ),
    'pimple/pimple' => 
    array (
      'pretty_version' => 'v3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '21e45061c3429b1e06233475cc0e1f6fc774d5b0',
      'dev-requirement' => false,
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
      'dev-requirement' => false,
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
      'dev-requirement' => false,
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
      'dev-requirement' => false,
    ),
    'psr/http-client-implementation' => 
    array (
      'dev-requirement' => false,
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
      'dev-requirement' => false,
    ),
    'psr/http-factory-implementation' => 
    array (
      'dev-requirement' => false,
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
      'dev-requirement' => false,
    ),
    'psr/http-message-implementation' => 
    array (
      'dev-requirement' => false,
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
      'dev-requirement' => false,
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
      'dev-requirement' => false,
    ),
    'qcloud/cos-sdk-v5' => 
    array (
      'pretty_version' => 'v1.3.4',
      'version' => '1.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '1b32aa422f6dffe4ea411e5095e4b0da9135551b',
      'dev-requirement' => false,
    ),
    'qiniu/php-sdk' => 
    array (
      'pretty_version' => 'v7.2.10',
      'version' => '7.2.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd89987163f560ebf9dfa5bb25de9bd9b1a3b2bd8',
      'dev-requirement' => false,
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
      'dev-requirement' => false,
    ),
    'songshenzong/support' => 
    array (
      'pretty_version' => '2.0.5',
      'version' => '2.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '34973c04ffcf226e503f1c3a69d30ac49f7621f6',
      'dev-requirement' => false,
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v2.8.52',
      'version' => '2.8.52.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a77e974a5fecb4398833b0709210e3d5e334ffb0',
      'dev-requirement' => false,
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7110338d81ce1cbc3e273136e4574663627037a7',
      'dev-requirement' => false,
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f048e612a3905f34931127360bdd2def19a5e582',
      'dev-requirement' => true,
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.17.1',
      'version' => '1.17.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4a5b6bba3259902e386eb80dd1956181ee90b5b2',
      'dev-requirement' => true,
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.4.10',
      'version' => '4.4.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '56b3aa5eab0ac6720dcd559fd1d590ce301594ac',
      'dev-requirement' => true,
    ),
    'topthink/framework' => 
    array (
      'pretty_version' => 'v6.0.7',
      'version' => '6.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db8fe22520a9660dd5e4c87e304034ac49e39270',
      'dev-requirement' => false,
    ),
    'topthink/think' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '74590d73961429e48439e11219bb2817ed2111ea',
      'dev-requirement' => false,
    ),
    'topthink/think-helper' => 
    array (
      'pretty_version' => 'v3.1.4',
      'version' => '3.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c28d37743bda4a0455286ca85b17b5791d626e10',
      'dev-requirement' => false,
    ),
    'topthink/think-multi-app' => 
    array (
      'pretty_version' => 'v1.0.14',
      'version' => '1.0.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ccaad7c2d33f42cb1cc2a78d6610aaec02cea4c3',
      'dev-requirement' => false,
    ),
    'topthink/think-orm' => 
    array (
      'pretty_version' => 'v2.0.33',
      'version' => '2.0.33.0',
      'aliases' => 
      array (
      ),
      'reference' => '35ca511a1e4d671b39f7afb4c887703c16ef6957',
      'dev-requirement' => false,
    ),
    'topthink/think-trace' => 
    array (
      'pretty_version' => 'v1.3',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd8da2e39df268ab8775013de699f0c3012e51318',
      'dev-requirement' => true,
    ),
    'topthink/think-worker' => 
    array (
      'pretty_version' => 'v3.0.5',
      'version' => '3.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc0ac655a0a2efda073ec18e67668bc02d17cabe',
      'dev-requirement' => false,
    ),
    'workerman/gateway-worker' => 
    array (
      'pretty_version' => 'v3.0.19',
      'version' => '3.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '720cb0f23c3ae5f0143b3457901e177dd3d54387',
      'dev-requirement' => false,
    ),
    'workerman/workerman' => 
    array (
      'pretty_version' => 'v3.5.31',
      'version' => '3.5.31.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b73ddc45b3c7299f330923a2bde23ca6e974fd96',
      'dev-requirement' => false,
    ),
    'xin/container' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '97bb67f87dd851545938a1f2fe0ffbd379e3ff81',
      'dev-requirement' => false,
    ),
    'xin/helper' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '02a58132dae2aea2d1c0b8e66f55125969224747',
      'dev-requirement' => false,
    ),
  ),
);
private static $canGetVendors;
private static $installedByVendor = array();







public static function getInstalledPackages()
{
$packages = array();
foreach (self::getInstalled() as $installed) {
$packages[] = array_keys($installed['versions']);
}

if (1 === \count($packages)) {
return $packages[0];
}

return array_keys(array_flip(\call_user_func_array('array_merge', $packages)));
}










public static function isInstalled($packageName, $includeDevRequirements = true)
{
foreach (self::getInstalled() as $installed) {
if (isset($installed['versions'][$packageName])) {
return $includeDevRequirements || empty($installed['versions'][$packageName]['dev-requirement']);
}
}

return false;
}













public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

$ranges = array();
if (isset($installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = $installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['version'])) {
return null;
}

return $installed['versions'][$packageName]['version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getPrettyVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return $installed['versions'][$packageName]['pretty_version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getReference($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['reference'])) {
return null;
}

return $installed['versions'][$packageName]['reference'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getRootPackage()
{
$installed = self::getInstalled();

return $installed[0]['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
self::$installedByVendor = array();
}





private static function getInstalled()
{
if (null === self::$canGetVendors) {
self::$canGetVendors = method_exists('Composer\Autoload\ClassLoader', 'getRegisteredLoaders');
}

$installed = array();

if (self::$canGetVendors) {
foreach (ClassLoader::getRegisteredLoaders() as $vendorDir => $loader) {
if (isset(self::$installedByVendor[$vendorDir])) {
$installed[] = self::$installedByVendor[$vendorDir];
} elseif (is_file($vendorDir.'/composer/installed.php')) {
$installed[] = self::$installedByVendor[$vendorDir] = require $vendorDir.'/composer/installed.php';
}
}
}

$installed[] = self::$installed;

return $installed;
}
}
