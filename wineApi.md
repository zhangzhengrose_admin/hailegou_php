## 商城接口文档

### 目录

<!-- toc -->
#### APP或H5登陆接口
- [APP应用或者H5登陆](#APP应用或者H5登陆)
- [APP应用或者H5推荐者注册](#APP应用或者H5推荐者注册)
#### 微信小程序登陆接口
- [A-微信小程序商城登陆-索取登陆牌照](#A微信小程序商城登陆索取登陆牌照)
- [B-微信小程序登陆接口](#B微信小程序登陆接口)
- [C-微信小程序登陆](#C微信小程序登陆)
#### 短信验证
- [发送短信验证码](#发送短信验证码)
#### 应用接口
- [获取用户信息](#获取用户信息)
- [获取账户资产](#获取账户资产)
- [获取当前用户待处理的订单数量](#获取当前用户待处理的订单数量)
- [商城首页](#商城首页)
- [获取商品详情](#获取商品详情)
- [商品分类](#商品分类)  
- [购物车列表](#购物车列表)
- [添加商品至购物车](#添加商品至购物车)
- [更新购物车](#更新购物车)
- [删除购物车商品](#删除购物车商品)
- [获取购物车商品总数量](#获取购物车商品总数量)
- [获取指定商品的服务与承诺](#获取指定商品的服务与承诺)
- [获取商品评论数据](#获取商品评论数据)
- [结算台订单信息](#结算台订单信息)
- [检查订单-立即购买模式](#检查订单立即购买模式)
- [检查订单-购物车购买模式](#检查订单购物车购买模式)
- [取消订单](#取消订单)
- [订单提交-立即购买](#订单提交立即购买)
- [订单提交-购物车购买](#订单提交购物车购买)
- [立即支付](#立即支付)
- [获取当前用户默认收货地址](#获取当前用户默认收货地址)
- [当前收货地址列表](#当前收货地址列表)
- [获取所有地区(树状)](#获取所有地区(树状))
- [添加收货地址](#添加收货地址)
- [设置默认收货地址](#设置默认收货地址)
- [我的订单列表](#我的订单列表)
- [我的订单详情](#我的订单详情)
- [立即收货](#立即收货)
- [申请成为经销商(废弃)](#申请成为经销商)
- [经销商信息查询(自己)](#经销商信息查询)
- [经销商收益明细](#经销商收益明细)
- [经销商等级](#经销商等级)  
- [我的经销商团队](#我的经销商团队)
- [经销商单成员消费明细](#经销商单成员消费明细)  
- [经销商消费记录](#经销商消费记录)  
- [实名认证](#实名认证)
- [获取实名认证状态](#获取实名认证状态)
- [提现申请](#提现申请)
- [当前用户提现明细](#当前用户提现明细)  
- [安卓版本更新](#安卓版本更新)
- [推荐海报](#推荐海报)
- [上传图片](#上传图片)
<!-- tocstop -->

### 正文

### APP应用或者H5登陆
- des 适用APP应用或者H5登陆模式，关于小程序登陆部分免看
- header
    - content-type **application/json**
- method  **POST**
- url     `/api/passport/login`
- params [json]
    - object form
        - bool isParty       false 是APP应用或者H5登陆
        - string mobile      手机号码
        - string smsCode     手机验证码
        - string platform    H5 平台类型  
        - object partyData {} 传空对象
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示（发送成功，请注意查收）
    - object data json数组，空值为[]
        - string userId     用户id
        - string token      API接口 Access-Token值

### APP应用或者H5推荐者注册
- des 推荐者注册
- header
    - content-type **application/json**
- method  **POST**
- url     `/api/passport/loginShare`
- params [json]
    - object form
        - string pid         推荐者号
        - bool isParty       false 是APP应用或者H5登陆
        - string mobile      手机号码
        - string smsCode     手机验证码
        - string platform    H5 平台类型
        - object partyData {} 传空对象
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示（发送成功，请注意查收）
    - object data json数组，空值为[]
        - string userId     用户id
        - string token      API接口 Access-Token值

### A微信小程序商城登陆索取登陆牌照
- des 微信小程序登陆前调取服务器登陆牌照
- header
    - content-type **application/json**
- method  **GET**
- url     `/api/passport/captcha`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - string base64 图形验证码数据
        - string key    票据，登陆需要用到
        - string md5    校验值

### B微信小程序登陆接口
- des 小程序登陆
- header
    - content-type **application/json**
- method  **POST**
- url     `/api/passport/mpWxLogin`
- params [json]
    - object form
        - string code wx.login接口获取 用户登录凭证（有效期五分钟）。开发者需要在开发者服务器后台调用 auth.code2Session，使用 code 换取 openid、unionid、session_key 等信息
        - object userInfo 从微信小程序接口获取用户数据
            - string avatarUrl 微信头像url
            - string city      城市
            - string country:  国家
            - string gender    性别 1男2女
            - string language  zh_CN
            - string nickName  微信昵称
            - string province  身份
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示
    - array data json数组，空值为[]

### C微信小程序登陆
- des 最后一步完成小程序登陆
- header
    - content-type **application/json**
- method  **POST**
- url     `/api/passport/login`
- params [json]
    - object form
        - bool isParty       true是否为小程序登陆
        - string mobile      手机号码
        - string smsCode     手机验证码
        - object partyData
            - string code
            - string oauth MP-WEIXIN 平台标识，接口Headers的参数platform的值
            - object userInfo 从微信小程序接口获取用户数据
                - string avatarUrl 微信头像url
                - string city      城市
                - string country:  国家
                - string gender    性别 1男2女
                - string language  zh_CN
                - string nickName  微信昵称
                - string province  身份
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示（发送成功，请注意查收）
    - object data json数组，空值为[]
        - string userId     用户id
        - string token      API接口 Access-Token值


### 发送短信验证码
- des 通过短信、图形验证码验证手机号码
- header
    - content-type **application/json**
- method  **POST**
- url     `/api/passport/sendSmsCaptcha`
- params [json]
    - object form
        - string captchaCode 输入图形验证码
        - string captchaKey  A接口票据即key值(每次需要重新获取)
        - string mobile      手机号码
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示（发送成功，请注意查收）
    - array data json数组，空值为[]

### 获取用户信息
- des 微信小程序获取用户信息
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/user/info`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示（发送成功，请注意查收）
        - array data json数组，空值为[]
            - object userInfo 用户信息对象

### 获取账户资产
- des 微信小程序获取账户资产
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/user/assets`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - array data json数组，空值为[]
            - object assets
                - decimal(7,2) balance 0.00
                - int coupon 0
                - int points 0

### 获取当前用户待处理的订单数量
- des 微信小程序获取获取当前用户待处理的订单数量
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/order/todoCounts`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - array data json数组，空值为[]
            - object counts 订单类型 (all全部 payment待付款 received待收货 delivery待发货 comment待评价)
                - int delivery 0 待收货数量
                - int payment 0  待付款数量
                - int received 0 待发货数量

### 商城首页
- des 获取商城首页数据，无需token即可访问
- header
    - content-type **application/json**
    - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/page/detail&pageId=0`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象,详情如下
```json
{
  "status": 200,
  "message": "success",
  "data": {
    "pageData": {
      "page": {
        "name": "页面设置",
        "type": "page",
        "params": {
          "name": "商城首页",
          "title": "酒业商城",
          "shareTitle": "分享标题"
        },
        "style": {
          "titleTextColor": "black",
          "titleBackgroundColor": "#ffffff"
        }
      },
      "items": [
        {
          "name": "搜索框",
          "type": "search",
          "params": {
            "placeholder": "请输入关键字进行搜索"
          },
          "style": {
            "textAlign": "left",
            "searchStyle": "square"
          }
        },
        {
          "name": "店铺公告",
          "type": "notice",
          "params": {
            "text": "酒业商城 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]",
            "link": null,
            "showIcon": true,
            "scrollable": true
          },
          "style": {
            "paddingTop": 0,
            "background": "#fffbe8",
            "textColor": "#de8c17"
          }
        },
        {
          "name": "商品组",
          "type": "goods",
          "params": {
            "source": "auto",
            "auto": {
              "category": 0,
              "goodsSort": "all",
              "showNum": 6
            }
          },
          "style": {
            "background": "#F6F6F6",
            "display": "list",
            "column": 1,
            "show": [
              "goodsName",
              "goodsPrice",
              "linePrice",
              "sellingPoint",
              "goodsSales"
            ]
          },
          "data": [
            {
              "goods_id": 2,
              "goods_name": "晟藏老酒 贵州茅台酒 辛丑牛年生肖纪念酒2021年53度500ML 单瓶装",
              "selling_point": "正品飞天茅台",
              "goods_image": "https://img.zhiwuyipin.com/10001/20210713/cd9e587e67cda2092bb4c069b7344e2f.jpg",
              "goods_price_min": "7000.00",
              "goods_price_max": "7000.00",
              "line_price_min": "7250.00",
              "line_price_max": "7250.00",
              "goods_sales": 6
            },
            {
              "goods_id": 1,
              "goods_name": "飞天茅台",
              "selling_point": "正品飞天茅台",
              "goods_image": "https://img.zhiwuyipin.com/10001/20210713/bc9c8890172827601705fb6016a6957e.jpg",
              "goods_price_min": "2890.00",
              "goods_price_max": "2890.00",
              "line_price_min": "2900.00",
              "line_price_max": "2900.00",
              "goods_sales": 2
            }
          ]
        }
      ]
    }
  }
}
```

### 获取商品详情
- des 获取获取商品详情数据，无需token即可访问
- header
    - content-type **application/json**
    - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/goods/detail`
- params
    - int goodsId 商品id
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 商品分类
- des 获取商品分类数据，无需token即可访问
- header
    - content-type **application/json（可选）**
    - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/goods/list`
- params
    - string sortType all综合排序 sales销量排序 price价格排序
    - bool sortPrice 价格排序 (true高到低 false低到高)
    - int categoryId 分类id，必传
    - string goodsName 商品名称搜索关键词,支持模糊索引
    - int page 页码，默认1
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 购物车列表
- des 获取购物车列表数据
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/cart/list`
- 无
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 添加商品至购物车
- des 添加商品到购物车保存
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/cart/add`
- params
    - int goodsId 商品id
    - int goodsNum 添加至购物车商品数量
    - string goodsSkuId 商品库存id 例如：1_5
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 更新购物车
- des 更新购物车商品梳理
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/cart/update`
- params
    - int goodsId 商品id
    - int goodsNum 添加至购物车商品数量
    - string goodsSkuId 商品库存id 例如：1_5
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 删除购物车商品
- des 删除购物车商品
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/cart/clear`
- params
    - array[int] cartIds 购物车id的数组
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 获取购物车商品总数量
- des 获取购物车商品总数量数据，无需token即可访问
- header
    - content-type **application/json**
    - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/cart/total`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 获取指定商品的服务与承诺
- des 获取指定商品的服务与承诺，无需token即可访问
- header
    - content-type **application/json**
    - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/goods.service/list`
- params
    - int goodsId 商品id
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 获取商品评论数据
- des 获取商品评论数据，无需token即可访问
- header
    - content-type **application/json**
    - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/comment/listRows`
- params
    - int goodsId 商品id
    - int limit   显示条数
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象
            - array list
            - int   total

### 结算台订单信息
- des 获取结算台订单信息
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/goods.service/list`
- params
    - int goodsId 商品id
    - string mode buyNow
    - int delivery 0
    - int couponId 0
    - int isUsePoints 0
    - int goodsNum 1
    - int goodsSkuId 0
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 检查订单立即购买模式
- des 先检查完订单后再提交订单
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/checkout/order`
- params
    - string mode buyNow(值固定)
    - int  delivery: 0  配送设置id，默认值为0
    - int  couponId: 0  优惠券id，默认值为0
    - int  isUsePoints: 0 积分抵扣点数，默认值为0
    - int  goodsId: 2   商品id
    - int  goodsNum: 1  要购买的商品数量
    - string  goodsSkuId: 1_5 商品sku唯一标识
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 检查订单购物车购买模式
- des 先检查完订单后再提交订单
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/checkout/order`
- params
    - string mode cart(值固定)
    - int  delivery: 0  配送设置id，默认值为0
    - int  couponId: 0  优惠券id，默认值为0
    - int  isUsePoints: 0 积分抵扣点数，默认值为0
    - int  cartIds:  0  购物车id，默认值为0,多个cartId用,号隔开，例如 2,3,4,8
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 取消订单
- des 取消已提交的订单
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/order/cancel`
- params
    - int  orderId  订单id
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 订单提交立即购买
- des 订单提交-立即购买
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/checkout/submit`
- params
    - string currency 币种 USD【美元】 KRW【韩元】 JPY【日元】 HKD【港元】EUR【欧元】
    - string client_ip IP地址
    - int couponId  优惠券id，默认值为0
        - int delivery  配送设置id，默认值为0
    - int goodsId  商品id
    - int goodsNum  要购买的商品数量
    - string goodsSkuId  商品sku唯一标识，例如1_5
    - int isUsePoints  积分抵扣点数，默认值为0
    - string mode  "buyNow"
    - int payType  支付类型 跨境支付30 微信支付20  余额支付10
    - string remark 备注，默认空值
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 订单提交购物车购买
- des 订单提交-购物车购买
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/checkout/submit`
- params
    - string currency 币种 USD【美元】 KRW【韩元】 JPY【日元】 HKD【港元】EUR【欧元】
    - string client_ip IP地址
    - int cartIds  购物车id，默认值为0,多个cartId用,号隔开，例如 2,3,4,8
    - int couponId  优惠券id，默认值为0
    - int delivery  配送设置id，默认值为0
    - int isUsePoints  积分抵扣点数，默认值为0
    - string mode  "cart"
    - int payType  支付类型 跨境支付30 微信支付20  余额支付10
    - string remark 备注，默认空值
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 立即支付
- des 立即支付已提交的订单
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/order/pay`
- params
    - string currency 币种 USD【美元】 KRW【韩元】 JPY【日元】 HKD【港元】EUR【欧元】
    - string client_ip IP地址
    - int  orderId  订单id
    - int  payType  支付类型 跨境支付30 微信支付20  余额支付10
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 获取当前用户默认收货地址
- des 获取当前用户默认收货地址
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/address/defaultId`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象


### 当前收货地址列表
- des 获取当前收货地址列表
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/address/list`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 获取所有地区(树状)
- des 获取所有地区(树状)列表
- header
    - content-type **application/json**
    - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/region/tree`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 添加收货地址
- des 添加收货地址数据
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/address/add`
- params [json]
    - object form
        - string detail 地址
        - string name 姓名
        - string phone 电话号码
        - array region
            - object
                - int value     获取所有地区接口获得城市id值
                - string label  获取所有地区接口获得城市名称
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 设置默认收货地址
- des 设置默认收货地址功能
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/address/setDefault`
- params
    - int addressId 收货地址id
- result
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 我的订单列表
- des 获取我的订单列表
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/order/list`
- params
    - string dataType all全部 payment待付款 received待发货 deliver待收货 comment待评价
    - int page 1第一页
- result
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 我的订单详情
- des 获取我的订单详情
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/order/detail`
- params
    - int orderId 订单id
- result
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 立即收货
- des 商家发货完毕，买家可以选择立即收货
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/order/receipt`
- params
    - int orderId 订单id
- result
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 申请成为经销商
- des 建立自己团队，注册成为经销商(废弃)
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Dealer/regist`
- params [json]
    - object form
        - int pid    经销商推荐者id
        - int idCard 经销商身份证号码
        - int id     经销商id（可选，若传入id则更新当前数据）
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 经销商信息查询
- des 经销商信息查询(自己)
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.Dealer/getder`
- params [无]
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 经销商收益明细
- des 经销商收益明细查询
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.Dealer/index`
- params 
    - int page 1 页面
    - int line 10 每页显示行数
    - int type 1经销商团队收益 2经销商直推收益
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 经销商等级
- des 经销商等级查询
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.Dealer/getDerLevel`
- params
    - int page 1 页面
    - int line 10 每页显示行数
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象
    
```json
{
    "status": 200,
    "message": "成功获取经销商等级列表",
    "data": {
        "list": [
            {
                "id": 1, // 经销商id  关联 perform_id
                "store_id": 10001,
                "sort": 0,
                "name": "普通经销商",
                "award": 0,
                "pcount": 1,
                "performance": 0,
                "direct_award": "1.00",
                "des": "普通经销商",
                "status": 1,
                "is_delete": 0,
                "create_time": "2021-07-30 17:44:55",
                "update_time": "2021-08-02 16:01:06"
            },
            {
                "id": 2,
                "store_id": 10001,
                "sort": 0,
                "name": "初级代理商",
                "award": 15,
                "pcount": 1,
                "performance": 500,
                "direct_award": "1.00",
                "des": "代理商",
                "status": 1,
                "is_delete": 0,
                "create_time": "2021-07-30 18:01:50",
                "update_time": "2021-08-16 16:19:39"
            }
        ]
    }
}
```

### 我的经销商团队
- des 我的经销商团队成员查询
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.Dealer/getDermember`
- params
    - int page 1 页面
    - int line 10 每页显示行数
    - int perform_id 经销商等级id，不传则查询自己所有等级的经销商
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象
    
```json
{
  "status": 200,
  "message": "success",
  "data": {
    "list": {
      "total": 2,
      "per_page": 10,
      "current_page": 1,
      "last_page": 1,
      "data": [
        {
          "id": 2,
          "store_id": 10001,
          "pid": 1,
          "user_id": 2,
          "perform_id": 1,
          "idCard": "430923199010282932",
          "authens": 0,
          "total_expend_money": "0.00",
          "pcount": 0,
          "revenue": "0.00",
          "cash": "0.00",
          "balance": "0.00",
          "is_delete": 0,
          "create_time": "2021-08-18 10:29:07",
          "update_time": "2021-08-18 10:29:07",
          "mobile": "13170309049",
          "nick_name": "131****9049",
          "name": "普通经销商",
          "award": 0,
          "des": "普通经销商"
        },
        {
          "id": 4,
          "store_id": 10001,
          "pid": 1,
          "user_id": 3,
          "perform_id": 0,
          "idCard": "74852156965551051207",
          "authens": 0,
          "total_expend_money": "0.00",
          "pcount": 0,
          "revenue": "1.00",
          "cash": "0.00",
          "balance": "0.00",
          "is_delete": 0,
          "create_time": "2021-09-06 17:58:17",
          "update_time": "2021-09-06 17:58:17",
          "mobile": "19198214927",
          "nick_name": "191****4927",
          "name": null,
          "award": null,
          "des": null
        }
      ]
    }
  }
}
```


### 经销商单成员消费明细
- des 经销商单成员消费明细查询
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.Dealer/orderDetail`
- params
    - int page 1 页面
    - int line 10 每页显示行数
    - int income_id 经销商收益明细的id值，必传
    - int user_id 经销商的用户id
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 经销商消费记录
- des 经销商结算后的消费记录,包含自己
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api//wine.Dealer/searchOrder`
- params
    - url?param
        - int page 经销商团队成员列表页码默认为1
    - object form
        - int line 经销商团队每页显示行数,默认15条
        - int line2 消费明细显示行数,默认15条
        - int perform_id 经销商等级id，不传则查询自己所有等级的经销商
        - int income_id  经销商收益明细的id值，必传
        - int page 经销商成员消费明细列表页码
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象
    
```json
{
    "status": 200,
    "message": "经销商团队消费记录获取成功",
    "data": {
        "list": {
          // 经销商自己消费记录
            "my": {
                "total": 1,
                "per_page": 3,
                "current_page": 1,
                "last_page": 1,
                "data": [
                    {
                        "id": 1,
                        "store_id": 10001,
                        "pid": 0,
                        "user_id": 1,
                        "perform_id": 2,
                        "idCard": "430528198708130039",
                        "total_expend_money": "4000.09",
                        "pcount": 1,
                        "revenue": "600.01",
                        "cash": "600.00",
                        "balance": "0.01",
                        "is_delete": 0,
                        "create_time": "2021-08-13 17:41:03",
                        "update_time": "2021-08-13 17:41:21",
                        "mobile": "18908459468",
                        "nick_name": "189****9468",
                        "name": "初级代理商",
                        "award": 15,
                        "des": "代理商",
                        "saleList": {
                            "total": 6,
                            "per_page": 1,
                            "current_page": 1,
                            "last_page": 6,
                            "data": [
                                {
                                    "order_id": 26,
                                    "order_no": "2021081799535255",
                                    "total_price": "0.01",
                                    "order_price": "0.01",
                                    "coupon_id": 0,
                                    "coupon_money": "0.00",
                                    "points_money": "0.00",
                                    "points_num": 0,
                                    "pay_price": "0.01",
                                    "update_price": {
                                        "symbol": "+",
                                        "value": "0.00"
                                    },
                                    "buyer_remark": "",
                                    "pay_type": 30,
                                    "pay_status": 20,
                                    "pay_time": "2021-08-17 10:59:28",
                                    "delivery_type": 10,
                                    "express_price": "0.00",
                                    "express_id": 0,
                                    "express_company": "",
                                    "express_no": "",
                                    "delivery_status": 10,
                                    "delivery_time": "1970-01-01 08:00:00",
                                    "receipt_status": 10,
                                    "receipt_time": "1970-01-01 08:00:00",
                                    "order_status": 10,
                                    "points_bonus": 0,
                                    "is_settled": 0,
                                    "transaction_id": "",
                                    "is_comment": 0,
                                    "order_source": 10,
                                    "order_source_id": 0,
                                    "user_id": 1,
                                    "is_delete": 0,
                                    "create_time": "2021-08-17 10:58:20",
                                    "award_price": 0.0015,
                                    "goods": [
                                        {
                                            "order_goods_id": 26,
                                            "goods_id": 1,
                                            "goods_name": "第八代五粮液52度500ml*2瓶浓香白酒商务宴请普五",
                                            "image_id": 2,
                                            "deduct_stock_type": 10,
                                            "spec_type": 10,
                                            "goods_sku_id": "0",
                                            "goods_props": null,
                                            "goods_no": "",
                                            "goods_price": "0.01",
                                            "line_price": "1998.00",
                                            "goods_weight": 0,
                                            "is_user_grade": 0,
                                            "grade_ratio": 0,
                                            "grade_goods_price": "0.00",
                                            "grade_total_money": "0.00",
                                            "coupon_money": "0.00",
                                            "points_money": "0.00",
                                            "points_num": 0,
                                            "points_bonus": 0,
                                            "total_num": 1,
                                            "total_price": "0.01",
                                            "total_pay_price": "0.01",
                                            "is_comment": 0,
                                            "order_id": 26,
                                            "user_id": 1,
                                            "goods_source_id": 0,
                                            "goods_image": "https://img.zhiwuyipin.com/10001/20210811/abe3f447a27c47f1263f1c28de3bc427.jpg"
                                        }
                                    ],
                                    "state_text": "待发货"
                                }
                            ]
                        }
                    }
                ]
            },
          // 其他经销商消费记录
            "other": {
                "total": 1,
                "per_page": 3,
                "current_page": 1,
                "last_page": 1,
                "data": [
                    {
                        "id": 2,
                        "store_id": 10001,
                        "pid": 1,
                        "user_id": 2,
                        "perform_id": 1,
                        "idCard": "430923199010282932",
                        "total_expend_money": "0.00",
                        "pcount": 0,
                        "revenue": "0.00",
                        "cash": "0.00",
                        "balance": "0.00",
                        "is_delete": 0,
                        "create_time": "2021-08-18 10:29:07",
                        "update_time": "2021-08-18 10:29:07",
                        "mobile": "13170309049",
                        "nick_name": "131****9049",
                        "name": "普通经销商",
                        "award": 0,
                        "des": "普通经销商",
                        "saleList": {
                            "total": 0,
                            "per_page": 1,
                            "current_page": 1,
                            "last_page": 0,
                            "data": []
                        }
                    }
                ]
            }
        }
    }
}
```


### 实名认证
- des 银行卡四要素实名认证查询
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/Authenticate/authen`
- params 
    - object form
        - string realName  真实姓名
        - string idCard    身份证号码
        - string client_ip 客户端ip地址
        - string accountNo 银行卡
        - string mobile    绑卡手机号
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 获取实名认证状态
- des 实名认证状态查询
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/Authenticate/getAuthenticate`
- params [无]
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 提现申请
- des 经销商申请提现
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.cash/applycash`
- params 
    - float cash_r 150.68 待申请提现金额
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 当前用户提现明细
- des 当前用户提现明细
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.cash/record`
- params
    - int page 1 
    - int line 10
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 安卓版本更新
- des 提供版号和安装包下载地址
- header
    - [无]
- method  **GET**
- url     `/api/Version/index`
- params
    - [无]
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象

### 推荐海报
- des 获取推荐海报相关资源
- header
    - content-type **application/json（可选）**
    - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/poster/share`
- params [无]
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象
    
```json
{
    "status": 200,
    "message": "success",
    "data": {
        "detail": {
            "url": "/share/index.html?pid=1", // 推荐链接
            "width": 400, //二维码宽度
            "left": 200, //二维码左边距
            "top": 600, //二维码顶部边距
            "derUrl": "https://img.zhiwuyipin.com/10001/20210811/21febebce3eec650666b77bdd45bdc66.jpg" // 海报背景图片链接
        }
    }
}
```


### 上传图片
- des 上传图片,直接返回可访问的图片url地址，每次上传图片url地址都会变化，原url地址仍可访问。需自己保存url地址，服务器不保存url
- header
    - content-type **application/json**
    - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Dealer/image`
- formData
    - int groupId 0 分组上传，默认0不分组
    - bin iFile 图片上传流媒体
- result [json]
    - int status 200
    - string message success
    - object data
        - int status 200成功 500/404失败
        - string message 消息提示
        - object data json对象
    
```html
data.fileInfo.external_url 为图片访问url
```
