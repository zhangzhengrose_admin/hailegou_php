-- 海乐购经销商配置表
drop table if EXISTS `yoshop_perform`;
CREATE TABLE `yoshop_perform` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11) unsigned      default 0 comment '商城ID',
`sort`          int(11) unsigned      DEFAULT '0'   COMMENT '排序号码',

`name`          varchar(100)  default '' comment '代理商标识名',
`award`         tinyint(2)   unsigned   default 0    COMMENT '奖励百分比，15即15%',
`pcount`        int(11)      unsigned   default 0  comment '团队成员个数',
`performance`   bigint(200)  unsigned   default 0  comment '业绩金额',
`direct_award`  decimal(11,2)  unsigned   default 0 comment '直推奖励金额',
`des`           varchar(100) default '' comment '描述',
`status`        tinyint(1)   default 1  comment '状态1正常0禁用',


`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned     DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `store_id`   (`store_id`)

) ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='海乐购合作商配置表' AUTO_INCREMENT=1 ;

-- 经销商成员表
drop table if EXISTS `yoshop_dermember`;
CREATE TABLE `yoshop_dermember` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11) unsigned       default 0  comment '商城ID',
`pid`           int(11) unsigned       default 0  comment '推荐者经销商id',
`user_id`       int(11) unsigned       default 0  comment '用户id',

`perform_id`    int(11) unsigned       default 0  comment '经销商配置id，用于经销商等级查询',
`perform_id_set`    int(11) unsigned       default 0  comment '经销商配置id，用于手动调整的经销商等级',
`idCard`        varchar(20)   default '' comment '经销商身份证号码',
`authens`       tinyint(1)    unsigned   default 0  comment '是否完成实名认证 1已实名0未实名',
`total_expend_money`  decimal(11,2)  unsigned   default 0 comment '团队销售额',
`pcount`        int(11)        unsigned   default 0  comment '团队成员个数',
`revenue`       decimal(11,2)  unsigned   default 0  comment '个人总收益',
`cash`          decimal(11,2)  unsigned   default 0  comment '个人提取总金额',
`balance`       decimal(11,2)  unsigned   default 0  comment '个人总余额',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
UNIQUE key `user_id`  (`user_id`),
key `pid`  (`pid`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商成员表' AUTO_INCREMENT=1 ;

-- 经销商收益明细表
drop table if EXISTS `yoshop_income`;
CREATE TABLE `yoshop_income` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11) unsigned       default 0  comment '商城ID',
`user_id`       int(11) unsigned       default 0  comment '用户id',
`revenue`       decimal(11,2)  unsigned   default 0  comment '收益',
`type`          tinyint(1)     unsigned   default 1  comment '收益类型 1经销商团队收益 2经销商直推收益',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商收益明细表' AUTO_INCREMENT=1 ;


-- 经销商提现申请表
drop table if EXISTS `yoshop_cash_request`;
CREATE TABLE `yoshop_cash_request` (
`id`            int(11) not null      AUTO_INCREMENT,
`user_id`       int(11) unsigned      default 0  comment '用户id',
`store_id`      int(11) unsigned      default 0  comment '商城ID',

`cash_r`        decimal(11,2)         unsigned  default 0  comment '提现申请金额',
`status`        tinyint(1)            default 0   comment '0待申请 1正在处理 2已提现 3驳回',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `store_id`   (`store_id`)

)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商提现申请表' AUTO_INCREMENT=1 ;

-- 经销商提现明细表
drop table if EXISTS `yoshop_cash`;
CREATE TABLE `yoshop_cash` (
`id`            int(11)       not null AUTO_INCREMENT,
`user_id`       int(11) unsigned       default 0  comment '用户id',
`store_id`      int(11) unsigned       default 0  comment '商城ID',

`cash`          decimal(11,2)          unsigned  default 0  comment '提现',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商提现明细表' AUTO_INCREMENT=1 ;

-- 用户扩展信息
drop table if EXISTS `yoshop_user_ext`;
CREATE TABLE `yoshop_user_ext` (
`id`            int(11)       not null AUTO_INCREMENT,
`user_id`       int(11) unsigned       default 0  comment '用户id',
`store_id`      int(11) unsigned       default 0  comment '商城ID',

`client_ip`     varchar(20)   default '' comment  '用户客户端ip地址',
`idCard`        varchar(20)   default '' comment  '身份证号码',
`realName`      varchar(10)   default '' comment  '真实姓名',
`authenticate`  text  comment  '认证信息',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `user_id`   (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户扩展信息' AUTO_INCREMENT=1 ;

-- app安卓版本更新表
drop table if EXISTS `yoshop_version`;
CREATE TABLE `yoshop_version` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11) unsigned       default 0  comment '商城ID',

`version`       varchar(20)   default '' comment  '版本号',
`url`           text comment  '安装包下载路径',
`remark`        text comment  '版本号',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='app安卓版本更新表' AUTO_INCREMENT=1 ;

-- 海报模板
drop table if EXISTS `yoshop_poster`;
CREATE TABLE `yoshop_poster` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11) unsigned       default 0  comment '商城ID',

`imagesIds`     varchar(40) default '' comment  '图片id',
`url`           text  comment  '分享链接',
`width`         int(11) unsigned   default 200 comment  '二维码宽度',
`left`         int(11)   default 0 comment  '二维码坐标',
`top`          int(11)   default 0 comment  '二维码坐标',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='海报模板表' AUTO_INCREMENT=1 ;
